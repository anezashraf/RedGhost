# RedGhost
Linux post exploitation framework designed to assist red teams in gaining persistence, reconnaissance and leaving no trace. 
![RG](https://user-images.githubusercontent.com/44454186/60026569-4c864800-968b-11e9-8d08-7a4d0bb333b3.PNG)
- Payloads
Function to generate various encoded reverse shells in
netcat, bash, python, php, ruby, perl
- lsWrapper 
Function to wrap the "ls" command with payload to run payload everytime "ls" is run for persistence 
- Crontab
Function to create cron job that downloads and runs payload every minute for persistence
- Clearlogs
Function to clear logs and make investigation with forensics difficult
- MassInfoGrab
Function to grab mass information on system
- BanIp
Function to BanIp
